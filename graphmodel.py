## from command line start tensorboard --logdir=path, to view the graph"

import tensorflow as tf
from tensorflow.python.platform import gfile

with tf.Session() as sess:
    model_filename ='/home/lua/Desktop/darkflow_project/built_graph/yolo_final.pb'
    with gfile.FastGFile(model_filename, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        g_in = tf.import_graph_def(graph_def)
log_dir='/home/lua/Desktop/darkflow_project/new'
train_writer = tf.summary.FileWriter(log_dir)
train_writer.add_graph(sess.graph)

