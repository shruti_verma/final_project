from __future__ import print_function
import argparse
import random
import os


def split_dataset(img_dir, val_per):
    wd = os.getcwd()
    all_files = wd + "/data/own/all_files.txt"
    train_txt = wd + "/data/own/train.txt"
    val_txt = wd + "/data/own/val.txt"
    with open(all_files, 'w') as f:
        for root, dirs, files in os.walk(str(img_dir) + "/"):
            for name in files:
                f.write(os.path.join(root, name) + "\n")

    with open(all_files, 'r') as f1:
        lines = f1.readlines()

    random.shuffle(lines)

    div = int(len(lines) * float(val_per))
    with open(train_txt, "w") as tf:
        tf.writelines(lines[div:])
    with open(val_txt, "w") as tv:
        tv.writelines(lines[:div])

    print("Done creating files")


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-fp", "--filepath", required=True, help="path to the images directory")
    ap.add_argument("-vr", "--valsplit", required=True, help="percentage split for validation data data in float")
    args = vars(ap.parse_args())

    image_path = args["filepath"]
    val_percentage = args["valsplit"]
    split_dataset(image_path, val_percentage)

