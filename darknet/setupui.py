# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'setup.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from __future__ import print_function
from PyQt4 import QtCore, QtGui
from functools import partial
from PyQt4.QtGui import QFileDialog, QPixmap
import datasetup

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(640, 480)
        MainWindow.setStyleSheet(_fromUtf8("background-color: rgb(38, 166, 154);"))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 131, 31))
        self.label.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);\n"
                                           "color: rgb(0, 0, 0);"))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 61, 31))
        self.label_2.setAutoFillBackground(False)
        self.label_2.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);\n"
                                             "color: rgb(0, 85, 127);"))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.pushButton = QtGui.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(390, 50, 31, 35))
        self.pushButton.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);"))
        self.pushButton.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("icons/ic_folder.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton.setIcon(icon)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(490, 140, 31, 35))
        self.pushButton_2.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);"))
        self.pushButton_2.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("icons/ic_create.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_2.setIcon(icon1)
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_3 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(220, 190, 71, 35))
        self.pushButton_3.setStyleSheet(_fromUtf8("color: rgb(0, 0, 0);\n"
                                                  "background-color: rgba(0, 0, 0, 50);"))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.label_6 = QtGui.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(10, 100, 131, 31))
        self.label_6.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);\n"
                                             "color: rgb(0, 0, 0);"))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(10, 140, 51, 31))
        self.label_7.setAutoFillBackground(False)
        self.label_7.setStyleSheet(_fromUtf8("color: rgb(0, 85, 127);\n"
                                             "background-color: rgba(0, 0, 0, 0);"))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_9 = QtGui.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(150, 140, 81, 31))
        self.label_9.setAutoFillBackground(False)
        self.label_9.setStyleSheet(_fromUtf8("color: rgb(0,85, 127);\n"
                                             "background-color: rgba(0, 0, 0, 0);"))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.label_11 = QtGui.QLabel(self.centralwidget)
        self.label_11.setGeometry(QtCore.QRect(320, 140, 41, 31))
        self.label_11.setAutoFillBackground(False)
        self.label_11.setStyleSheet(_fromUtf8("color: rgb(0,85, 127);\n"
                                              "background-color: rgba(0, 0, 0, 0);"))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.label_13 = QtGui.QLabel(self.centralwidget)
        self.label_13.setGeometry(QtCore.QRect(10, 190, 191, 31))
        self.label_13.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);\n"
                                              "color: rgb(0, 0, 0);"))
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.label_14 = QtGui.QLabel(self.centralwidget)
        self.label_14.setGeometry(QtCore.QRect(10, 240, 111, 31))
        self.label_14.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);\n"
                                              "color: rgb(0, 0, 0);"))
        self.label_14.setObjectName(_fromUtf8("label_14"))
        self.pushButton_4 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(450, 240, 31, 35))
        self.pushButton_4.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 0);"))
        self.pushButton_4.setText(_fromUtf8(""))
        self.pushButton_4.setIcon(icon)
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_5 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(20, 290, 102, 35))
        self.pushButton_5.setStyleSheet(_fromUtf8("color: rgb(0, 0, 0);"
                                                  "background-color: rgba(0, 0, 0, 30);"))
        self.pushButton_5.setText(_fromUtf8("Annotations"))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_5"))
        self.lineEdit = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(60, 140, 71, 31))
        self.lineEdit.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 50);\n"
                                              "color: rgb(0, 0, 0);"))
        self.lineEdit.setText(_fromUtf8(""))
        self.lineEdit.setFrame(False)
        self.lineEdit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.lineEdit_2 = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(230, 140, 71, 31))
        self.lineEdit_2.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 50);\n"
                                                "color: rgb(0, 0, 0);"))
        self.lineEdit_2.setText(_fromUtf8(""))
        self.lineEdit_2.setFrame(False)
        self.lineEdit_2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_3 = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit_3.setGeometry(QtCore.QRect(370, 140, 71, 31))
        self.lineEdit_3.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 50);\n"
                                                "color: rgb(0, 0, 0);"))
        self.lineEdit_3.setText(_fromUtf8(""))
        self.lineEdit_3.setFrame(False)
        self.lineEdit_3.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.lineEdit_4 = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit_4.setGeometry(QtCore.QRect(80, 50, 301, 31))
        self.lineEdit_4.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 50);\n"
                                                "color: rgb(0, 0, 0);"))
        self.lineEdit_4.setText(_fromUtf8(""))
        self.lineEdit_4.setFrame(False)
        self.lineEdit_4.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lineEdit_4.setObjectName(_fromUtf8("lineEdit_4"))
        self.lineEdit_5 = QtGui.QLineEdit(self.centralwidget)
        self.lineEdit_5.setGeometry(QtCore.QRect(130, 240, 301, 31))
        self.lineEdit_5.setStyleSheet(_fromUtf8("background-color: rgba(0, 0, 0, 50);\n"
                                                "color: rgb(0, 0, 0);"))
        self.lineEdit_5.setText(_fromUtf8(""))
        self.lineEdit_5.setFrame(False)
        self.lineEdit_5.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.lineEdit_5.setObjectName(_fromUtf8("lineEdit_5"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Model Setup", None))
        self.label.setText(_translate("MainWindow", "Set Dataset Path", None))
        self.label_2.setText(_translate("MainWindow", " Images", None))
        self.pushButton_3.setText(_translate("MainWindow", "Merge", None))
        self.label_6.setText(_translate("MainWindow", " Split Dataset(%)", None))
        self.label_7.setText(_translate("MainWindow", "  Train", None))
        self.label_9.setText(_translate("MainWindow", " Validation", None))
        self.label_11.setText(_translate("MainWindow", " Test", None))
        self.label_13.setText(_translate("MainWindow", " Merge (train + validation)", None))
        self.label_14.setText(_translate("MainWindow", "  Create Labels", None))
        self.pushButton.clicked.connect(partial(self.getfilepath, 0))
        self.pushButton_4.clicked.connect(partial(self.getfilepath, 1))
        self.pushButton_2.clicked.connect(self.split_data)
        self.pushButton_5.clicked.connect(self.create_labels)

    def getfilepath(self, value):
        if value == 0:
            fname = QFileDialog.getExistingDirectory()
            self.lineEdit_4.setText(fname)
        elif value == 1:
            fname = QFileDialog.getExistingDirectory()
            self.lineEdit_5.setText(fname)

    def split_data(self):
        if self.lineEdit_4.text() and self.lineEdit.text() and self.lineEdit_2.text() != "":
            datasetup.split_dataset(self.lineEdit_4.text(), train_per=self.lineEdit.text(),
                                    val_per=self.lineEdit_2.text())
            # msg = QtGui.QMessageBox()
            # msg.setIcon(QtGui.QMessageBox.Information)
            # msg.setText("This is a message box")
            # msg.setInformativeText("This is additional information")
            # msg.setWindowTitle("MessageBox demo")
        else:
            raise ValueError("Image directory field is empty")

    def create_labels(self):
        if self.lineEdit_5.text() != "":
            datasetup.convert_darknet_annot(str(self.lineEdit_5.text()), str(self.lineEdit_4.text()))


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
