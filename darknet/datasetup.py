from __future__ import print_function
import os
import xml.etree.ElementTree as ET
import xmltodict
import random

classes = ['sale', 'tonnoRio', 'risotti', 'granRisparmio', 'skipper', 'colgate', 'scotti']
img_mode = ['train', 'val']



def convert(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[1]) / 2.0
    y = (box[2] + box[3]) / 2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x *= dw
    w *= dw
    y *= dh
    h *= dh
    return (x, y, w, h)


def split_dataset(dir, train_per, val_per):
    wd = os.getcwd()
    all_files = wd + "/data/own/all_files.txt"
    train_txt = wd + "/data/own/train.txt"
    val_txt = wd + "/data/own/val.txt"
    with open(all_files, 'w') as f:
        for root, dirs, files in os.walk(str(dir) + "/"):
            for name in files:
                f.write(os.path.join(root, name) + "\n")

    with open(all_files, 'r') as f1:
        lines = f1.readlines()

    random.shuffle(lines)
    div = int(len(lines) * float(val_per))
    with open(train_txt, "w") as tf:
        tf.writelines(lines[div:])
    with open(val_txt, "w") as tv:
        tv.writelines(lines[:div])

    print ("Done splitting the data")


def convert_darknet_annot(annot_dir, save_img_dir):
    for root, dirs, files in os.walk(annot_dir+"/"):
        for name in files:
            img_name = name.split(".")[0]
            input_xml = open(os.path.join(root, name))
            output_txt = open(save_img_dir+ "/" + img_name + ".txt", 'w')
            tree = ET.parse(input_xml)
            node = tree.getroot()
            size = node.find('size')
            w = int(size.find('width').text)
            h = int(size.find('height').text)

            for obj in node.iter('object'):
                difficult = obj.find('difficult').text
                cls = obj.find('name').text
                if cls not in classes or int(difficult) == 1:
                    continue
                cls_id = classes.index(cls)
                xmlbox = obj.find('bndbox')
                b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text),
                     float(xmlbox.find('ymax').text))
                bb = convert((w, h), b)
                output_txt.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')

    print ("Done creating annotation txt files")

