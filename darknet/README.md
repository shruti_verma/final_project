# README #

Products detection and recognition. The object detection is about not only finding the class of an object but also localizing
the object in the image, "where" needs to be taken care of. The model architecture needs to be good enough for real time detection.
For the classification model Darknet19 has been used as base to Yolov2. Darknet has been configured with CUDA and OPENCV support.

Summary with full steps and configuration present in Products detection and Recognition pdf. All the code is Python 2.7 supported and works with 3.0 compatibilty

## Dependencies
1. PyQt4 for using GUI interface
2. Fork the repo: https://github.com/pjreddie/darknet.git
3. Opencv installed with libs in path of pkgconfig.
4. Cuda installed and export it to environmental path or add to bashrc file

# Fork with changes
1. For the dataset configuration GUI interface (PyQt) has been provided which can be used by running setupui.py file 
in parent directory. Its easy to use, allows to give paths to respective folder of images and to save annotations files in the format required by darknet.
Can split the image dataset by giving in values.  
3. LabelImg tool has been used to label custom classes in the images, details saved in XML format
4. Preprocessing annotations required by darknet, code has been added and functional with GUI interface
5. Need to add preprocessing code for non GUI interface.
6. For CUDA and OPENCV support, values have to be modified in Makefile. By default GPU=0, OPENCV=0, CUDNN=0. As per requirements 
vales have to be changed to 1.

