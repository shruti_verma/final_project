from __future__ import print_function
import os
import argparse
import xml.etree.ElementTree as ET

classes = ['sale', 'tonnoRio', 'risotti', 'granRisparmio', 'skipper', 'colgate', 'scotti']


def convert(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[1]) / 2.0
    y = (box[2] + box[3]) / 2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x *= dw
    w *= dw
    y *= dh
    h *= dh
    return (x, y, w, h)


def convert_darknet_annot(input_path, output_path):
    for root, dirs, files in os.walk(input_path + "/"):
        for name in files:
            img_name = name.split(".")[0]
            input_xml = open(os.path.join(root, name))
	    print (input_xml)
            output_txt = open(output_path + "/" + img_name + ".txt", 'w')
            print (output_txt)
            tree = ET.parse(input_xml)
            node = tree.getroot()
            size = node.find('size')
            w = int(size.find('width').text)
            h = int(size.find('height').text)
            for obj in node.iter('object'):
                difficult = obj.find('difficult').text
                cls = obj.find('name').text
                if cls not in classes or int(difficult) == 1:
                    continue
                cls_id = classes.index(cls)
                xmlbox = obj.find('bndbox')
                b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text),
                     float(xmlbox.find('ymax').text))
                bb = convert((w, h), b)
                output_txt.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')

    print ("Done creating labels for each image file")


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", required=True, help="path to the annotation xml directory")
    ap.add_argument("-o", "--output", required=True, help="path to directory to save labels txt for images")
    args = vars(ap.parse_args())

    annot_path = args["input"]
    output_path = args["output"]
    convert_darknet_annot(annot_path, output_path)
