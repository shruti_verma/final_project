# README #
### What is this repository for? ###
Trained model on Darknet neural net based on C, the main issue encounterd was setup with Opencv libs. Though it was building on my 
minium configuration system but giving error of segmentation fault. So in order to use the trained model on 
the custom products, for image and video test processing, I had to port yolo cfg model and weights to Darkflow, a project of darknet 
based on Tensorflow. Few test images and test video is in parent directory. 
Darknet folder contains all the files and codes that were used to train the model and visualization pngs of trained model

###
Dependicies : Forked repo https://github.com/thtrieu/darkflow.git 

Tested with following library (versions given)
1. Cython : pip insall Cython (used 0.25.2 version)
2. Python 2.7.12 or Python 3.x
3. Tensorflow (1.2.1)
4. Numpy (1.13.1)

### How to setup
I have already configured following steps in the repo

1. clone darkflow from github and cd darkflow
2. Made changes to loader.py (utils folder) file as the offset values of darknet model differs by 4. Need to change the following line to make 
it work with Darkflow so that it can parse the cfg model and weights.
Error: AssertionError: expect 268263452 bytes, fount 268263456
Solution: change the line in loader.py
Original line: self.offset = 16 
Changed line: self.offset = 20
3. Made changes to predict.py file in yolov2 to show predictions with class labels
conf = mess + ": " + str(round((confidence * 100), 2))
cv2.putText(imgcv, conf, (left, top - 12),0, 1e-3 * h, colors[max_indx],thick//3)
4. Now run: python setup.py build_ext --inplace
5. Change the labels.txt with the class names of your custom products, like we did in Darknet
6. You can find trained model under cfg/yolo_final.cfg and weights in parent directory
7. ./flow --imgdir sample_img/ --model cfg/yolo_final.cfg --load yolo_final.weights
This will process two test images in sample_img folder and save the result images in out folder
8. Now for the test video processing.
./flow --model cfg/yolo_final.cfg --load yolo_final.weights --demo path_to_video_file --saveVideo
Opencv will preprocess each frame to predict class and confidence and then put text, bounding boxes, saving to a video file. 
Video test file for products is in parent directory. Preprocess and Postprocess code in predict.py file, yolov2 folder.
9. ./flow --model cfg/yolo_final.cfg --load yolo_final.weights --demo path_to_video_file --gpu 1.0
10. Code to test custom script with opencv and darkflow, check checkflow.py

## Issued faced by me with opencv in darknet, that led me to port model in Darkflow.
1. Couldn't find opencv libs and packge directory in PATH.
2. Didn't work on exporting the PATH
3. Tried creating opencv.pc file in pkgconfig but didn't have permissions to do so.
4. Tried manually adding libs and include dirs in Makfile but still found compilation errors
5. It compiled perfectly in my system but had segmentation fault errors. 
